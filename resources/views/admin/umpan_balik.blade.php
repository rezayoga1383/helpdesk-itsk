@extends('admin.template.main')

@section('title', 'Data Umpan Balik - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-4" id="top-content">
                            <h6 class="card-title m-0">Data Umpan Balik</h6>
                            <button type="button" class="btn btn-danger btn-sm btn-icon-text" id="bt-del"><i
                                    class="link-icon" data-feather="x-square"></i> Hapus Data</button>
                        </div>
                        <div class="table-responsive">
                            <table id="TabelUmpanBalik" class="table hover stripe" style="width:100%">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" class="form-check-input check-all"></th>
                                        <th>Nama</th>
                                        <th>Status</th>
                                        <th>Email</th>
                                        <th>No Telp</th>
                                        <th>Pesan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div id="modalDetail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalDetailLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDetailLabel">Detail Umpan Balik</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="mb-3">
                            <label for="detailNama" class="form-label">Nama</label>
                            <input type="text" class="form-control" id="detailNama" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailStatus" class="form-label">Status</label>
                            <input type="text" class="form-control" id="detailStatus" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailEmail" class="form-label">Email</label>
                            <input type="email" class="form-control" id="detailEmail" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailNoTelp" class="form-label">No Telp</label>
                            <input type="text" class="form-control" id="detailNoTelp" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailPesan" class="form-label">Pesan</label>
                            <textarea class="form-control" id="detailPesan" rows="3" readonly></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        var tabelUmpanBalikAdmin;
        $(document).ready(function() {
            tabelUmpanBalikAdmin = $('#TabelUmpanBalik').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('dataadmumpanbalik') }}",
                "createdRow": function(row, data, dataIndex) {
                    $(row).attr('id', data.id);
                },
                "columns": [{
                        "data": null,
                        render: function(data, type, row, meta) {
                            return '<input type="checkbox" class="form-check-input check">';
                        },
                        orderable: false,
                        searchable: false
                    },
                    {
                        "data": "nama"
                    },
                    {
                        "data": "status"
                    },
                    {
                        "data": "email"
                    },
                    {
                        "data": "no_telepon",
                        render: function(data, type, row, meta) {
                            return '<a href="https://wa.me/' + data + '" target="_blank">' + data +
                                '</a>';
                        }
                    },
                    {
                        "data": "pesan"
                    },
                    {
                        "data": null,
                        render: function(data, type, row, meta) {
                            return '<button type="button" class="btn btn-secondary btn-sm btn-icon-text bt-detail" ' +
                                'data-id="' + row.id + '" ' +
                                'data-nama="' + row.nama + '" ' +
                                'data-status="' + row.status + '" ' +
                                'data-email="' + row.email + '" ' +
                                'data-notelepon="' + row.no_telepon + '" ' +
                                'data-pesan="' + row.pesan + '">' +
                                '<i class="link-icon" data-feather="eye"></i>' +
                                '</button>';
                        }
                    },
                ],
                "aLengthMenu": [
                    [10, 30, 50, -1],
                    [10, 30, 50, "All"]
                ],
                "iDisplayLength": 10,
                "language": {
                    search: "",
                    "paginate": {
                        "previous": "Sebelumnya",
                        "next": "Selanjutnya"
                    },
                    info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    search: "Cari:",
                    lengthMenu: "Tampilkan _MENU_ entri",
                    zeroRecords: "Tidak ditemukan data yang sesuai",
                    infoEmpty: "Menampilkan 0 sampai 0 dari 0 entri",
                    infoFiltered: "(disaring dari _MAX_ entri keseluruhan)"
                },
                "responsive": true,
                drawCallback: function(settings) {
                    feather.replace();
                },
                initComplete: function() {
                    feather.replace();
                }
            });

            // Customize search input and length select
            $('#TabelUmpanBalik').each(function() {
                var datatable = $(this);
                var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
                search_input.attr('placeholder', 'Cari');
                search_input.removeClass('form-control-sm');
                var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
                length_sel.removeClass('form-control-sm');
            });

            // Check all checkboxes functionality
            $('.check-all').on('change', function() {
                var isChecked = $(this).is(':checked');
                $('.check').prop('checked', isChecked);
            });

            // Display detail modal with data
            $(document).on('click', '.bt-detail', function() {
                var modal = $('#modalDetail');
                modal.find('#detailNama').val($(this).data('nama'));
                modal.find('#detailStatus').val($(this).data('status'));
                modal.find('#detailEmail').val($(this).data('email'));
                modal.find('#detailNoTelp').val($(this).data('notelepon'));
                modal.find('#detailPesan').val($(this).data('pesan'));
                modal.modal('show');
            });

            // Delete selected entries
            $('#bt-del').on('click', function() {
                var selectedIDs = [];
                $('.check:checked').each(function() {
                    selectedIDs.push($(this).closest('tr').attr('id'));
                });

                if (selectedIDs.length > 0) {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Anda Yakin?',
                        text: 'Data Yang Dihapus Tidak Dapat Dikembalikan!',
                        showCancelButton: true,
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: "{{ route('hapusumpanbalik') }}",
                                type: "POST",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    ids: selectedIDs
                                },
                                success: function(response) {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Berhasil! 👌🥳',
                                        text: response.message,
                                        confirmButtonText: 'OK'
                                    });
                                    tabelUmpanBalikAdmin.ajax.reload();
                                },
                                error: function(xhr) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Gagal! ✋😌',
                                        text: xhr.responseJSON.message,
                                        confirmButtonText: 'OK'
                                    });
                                },
                            });
                        }
                    })
                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Peringatan!',
                        text: 'Pilih Data Yang Akan Dihapus!',
                        confirmButtonText: 'OK'
                    });
                }
            });
        });

        // Adjust columns and responsiveness on window resize
        $(window).resize(function() {
            $('#TabelUmpanBalik').DataTable().columns.adjust().responsive.recalc();
        });
    </script>
@endpush

@push('style')
    <style>
        .link-icon {
            max-width: 20px;
        }

        .page-item.active .page-link {
            background-color: #14A44D !important;
            border-color: #14A44D !important;
            color: white !important;
        }

        .page-link {
            color: #333333 !important;
        }

        #TabelUmpanBalik thead th:first-child {
            cursor: default;
        }

        #TabelUmpanBalik thead th:first-child::after,
        #TabelUmpanBalik thead th:first-child::before {
            display: none !important;
            pointer-events: none;
        }

        #TabelUmpanBalik td,
        #TabelUmpanBalik th {
            text-align: center;
        }

        #TabelUmpanBalik td.child {
            text-align: left;
        }

        .dataTables_empty {
            text-align: center !important;
        }

        @media only screen and (max-width: 768px) {
            #TabelUmpanBalik td {
                white-space: normal;
                word-wrap: break-word;
            }

            #TabelUmpanBalik_filter {
                margin-top: 10px;
            }
        }

        @media only screen and (max-width: 476px) {
            #top-content {
                flex-direction: column;
            }

            #bt-del {
                margin-top: 10px;
                width: 72%;
            }

        }
    </style>
@endpush
