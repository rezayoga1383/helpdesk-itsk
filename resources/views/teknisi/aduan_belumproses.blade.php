@extends('teknisi.template.main')

@section('title', 'Data Aduan Belum diproses - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-4" id="top-content">
                            <h6 class="card-title m-0">Data Tiket Pengaduan</h6>
                            <div class="d-flex align-items-center flex-wrap text-nowrap" id="bt-group">
                                <div class="input-group date datepicker wd-200 me-2 mb-2 mb-md-0" id="dashboardDate">
                                    <span class="input-group-text input-group-addon bg-transparent border-success"><i
                                            data-feather="calendar" class=" text-success"></i></span>
                                    <input type="text" class="form-control border-success bg-transparent" id="bt-date">
                                </div>
                                <button type="button" class="btn btn-success btn-icon-text mb-2 mb-md-0 text-light"
                                    id="bt-download">
                                    <i class="btn-icon-prepend" data-feather="download-cloud"></i>
                                    Download Laporan
                                </button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="tabelAduanBelumProses" class="table hover stripe" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Posisi</th>
                                        <th>Telepon</th>
                                        <th>Kategori Masalah</th>
                                        <th>Deskripsi Masalah</th>
                                        <th>Tanggal Masuk</th>
                                        <th>File Tiket</th>
                                        <th>Tanggapan</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div id="modalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditLabel">Estimasi Pengerjaan
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="mb-3">
                            <label for="tanggalMulai" class="form-label">Tanggal Mulai</label>
                            <input type="date" class="form-control" id="tanggalMulai">
                        </div>
                        <div class="mb-3">
                            <label for="tanggalSelesai" class="form-label">Tanggal Selesai</label>
                            <input type="date" class="form-control" id="tanggalSelesai" disabled>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success" id="btn-simpan" data-tiket-id=""><i
                            class="fas fa-save"></i>Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalImage" tabindex="-1" aria-labelledby="modalImageLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalImageLabel">File Image</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img id="modalImageContent" src="" alt="File Image" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        function openModal(imageSrc) {
            document.getElementById('modalImageContent').src = imageSrc;
            var modal = new bootstrap.Modal(document.getElementById('modalImage'));
            modal.show();
        }

        var tabelAduanBelumProses;
        $(document).ready(function() {
            tabelAduanBelumProses = $('#tabelAduanBelumProses').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('datatksaduanbelumproses') }}",
                "columns": [{
                        "data": null,
                    },
                    {
                        "data": "nama"
                    },
                    {
                        "data": "posisi"
                    },
                    {
                        "data": "no_telepon",
                        render: function(data, type, row, meta) {
                            return '<a href="https://wa.me/' + data + '" target="_blank">' + data +
                                '</a>';
                        }
                    },
                    {
                        "data": "kategori_laporan"
                    },
                    {
                        "data": "judul_tiket"
                    },
                    {
                        "data": "tanggal_masuk"
                    },
                    {
                        "data": "file_tiket",
                        render: function(data, type, row, meta) {
                            if (data !== '---') {
                                return '<button type="button" class="btn btn-secondary btn-sm btn-icon-text" onclick="openModal(\'/storage/' +
                                    data + '\')">Lihat File</button>';
                            } else {
                                return '---';
                            }
                        }
                    },
                    {
                        "data": null,
                        render: function(data, type, row, meta) {
                            return '<button id="bt-proses" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modalEdit" data-tiket-id="' +
                                row.id + '"> Kerjakan </button>';
                        }
                    },
                ],
                "aLengthMenu": [
                    [10, 30, 50, -1],
                    [10, 30, 50, "All"]
                ],
                "iDisplayLength": 10,
                "order": [
                    [6, "desc"]
                ],
                "language": {
                    search: "",
                    "paginate": {
                        "previous": "Sebelumnya",
                        "next": "Selanjutnya"
                    },
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    "search": "Cari:",
                    "lengthMenu": "Tampilkan _MENU_ entri",
                    "zeroRecords": "Belum ada tiket yang dikirim oleh Admin!",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                    "infoFiltered": "(disaring dari _MAX_ entri keseluruhan)"
                },
                "responsive": true,
                "columnDefs": [{
                    "orderable": false,
                    "targets": 0
                }]
            });

            $('#tabelAduanBelumProses').DataTable().on('order.dt search.dt', function() {
                $('#tabelAduanBelumProses').DataTable().column(0, {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();

            $('#tabelAduanBelumProses').each(function() {
                var datatable = $(this);
                var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
                search_input.attr('placeholder', 'Cari');
                search_input.removeClass('form-control-sm');
                var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
                length_sel.removeClass('form-control-sm');
            });
        });
        $(window).resize(function() {
            $('#tabelAduanBelumProses').DataTable().columns.adjust().responsive.recalc();
        })

        var today = new Date(new Date().getTime() + (7 * 60 * 60 * 1000)).toISOString().split('T')[0];

        $("#tanggalMulai").change(function() {
            $('#tanggalSelesai').prop("disabled", false);

            var tanggalMulai = $(this).val();
            $('#tanggalSelesai').val(tanggalMulai);
        });

        $('#modalEdit').on('hidden.bs.modal', function(e) {
            $("#tanggalSelesai").prop('disabled', true);
            $('#tanggalSelesai').val('');
            $('#tanggalMulai').val('');
        });

        var inputMulai = document.getElementById("tanggalMulai");
        var inputSelesai = document.getElementById("tanggalSelesai");

        inputMulai.setAttribute("min", today);

        inputMulai.addEventListener("change", function() {
            var mulaiDate = new Date(inputMulai.value);
            var minSelesaiDate = new Date(mulaiDate.getTime() + (24 * 60 * 60 * 1000)); // Menambahkan 1 hari
            var minSelesaiISO = minSelesaiDate.toISOString().split('T')[0];
            inputSelesai.setAttribute("min", minSelesaiISO);
        });

        var todayMulai = new Date(new Date().getTime() + (7 * 60 * 60 * 1000)).toISOString().split('T')[0];
        inputSelesai.setAttribute("min", todayMulai);

        document.getElementById('bt-download').addEventListener('click', function() {
            var selectedDate = document.getElementById('bt-date').value;
            window.location.href = '/download-excel-teknisi-belum-proses?date=' + encodeURIComponent(selectedDate);
        });

        $(document).on('click', '#bt-proses', function(event) {
            event.preventDefault();

            $('#modalEdit').modal('show');
            var button = $(event.currentTarget);
            var tiketId = button.data('tiket-id');

            $('#btn-simpan').attr('data-tiket-id', tiketId);
        });

        $('#btn-simpan').click(function(event) {
            event.preventDefault();
            var tiketId = $(this).data('tiket-id');
            var tanggalMulai = $('#tanggalMulai').val();
            var tanggalSelesai = $('#tanggalSelesai').val();

            $.ajax({
                type: 'POST',
                url: '/kirim-tiket-belum-proses',
                data: {
                    _token: '{{ csrf_token() }}',
                    tiketId: tiketId,
                    tanggalMulai: tanggalMulai,
                    tanggalSelesai: tanggalSelesai,
                },
                success: function(response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Berhasil! 👌🥳',
                        text: response.message,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $('#modalEdit').modal('hide');
                        }
                    });
                    tabelAduanBelumProses.ajax.reload();
                },
                error: function(xhr, status, error) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Gagal! ✋😌',
                        text: 'Pilih Tanggal Mulai Dan Tanggal Selesai!',
                        confirmButtonText: 'OK'
                    });
                }
            });
        });
    </script>
@endpush

@push('style')
    <style>
        .page-item.active .page-link {
            background-color: #14A44D !important;
            border-color: #14A44D !important;
            color: white !important;
        }

        .page-link {
            color: #333333 !important;
        }


        #tabelAduanBelumProses td,
        #tabelAduanBelumProses th {
            text-align: center;
        }

        #tabelAduanBelumProses td.child {
            text-align: left;
        }


        @media (max-width: 768px) {
            #top-content {
                flex-direction: column;
            }

            #bt-group {
                justify-content: center;
                margin-top: 10px;
                margin-bottom: -20px;
            }

            #bt-download {
                display: block;
                width: 60%;
            }

            #tabelAduanBelumProses td {
                white-space: normal;
                word-wrap: break-word;
            }

            #tabelAduanBelumProses_filter {
                margin-top: 10px;
            }
        }

        @media (max-width: 468px) {
            #bt-download {
                width: 80%;
            }
        }

        @media (max-width: 384px) {
            #bt-download {
                width: 90%;
            }
        }
    </style>
@endpush
