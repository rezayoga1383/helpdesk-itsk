<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Profil;
use App\Models\Pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class PenggunaController extends Controller
{
    public function index()
    {
        $data = Pengguna::select(['id', 'email', 'role','status'])->get(); // Pastikan 'status' di sini
        return Datatables::of($data)->make(true);
    }

    public function storePengguna(Request $request)
    {
            // Validasi input untuk pengguna
            $request->validate([
                'email'    => 'required|email|unique:pengguna,email',
                'password' => 'required',
                'role'     => 'required|in:Super Admin,Admin,Teknisi',
                'status'   => 'required|in:Aktif,Tidak Aktif',
                'nama_lengkap' => 'required',
                'no_telepon'   => 'required',
            ], [
                'email.required' => 'Email wajib diisi.',
                'email.email' => 'Format email tidak valid.',
                'email.unique' => 'Email sudah terdaftar.',
                'password.required' => 'Password wajib diisi.',
                'role.required' => 'Role wajib diisi.',
                'role.in' => 'Role tidak valid. Pilihan yang valid adalah: Super Admin, Admin, Teknisi.',
                'status.required' => 'Status wajib diisi.',
                'status.in' => 'Status tidak valid. Pilihan yang valid adalah: Aktif, Tidak Aktif.',
                'nama_lengkap.required' => 'Nama lengkap wajib diisi.',
                'no_telepon.required' => 'Nomor telepon wajib diisi.',
            ]);

            // Buat pengguna baru
            $pengguna = new Pengguna;
            $pengguna->email = $request->email;
            $pengguna->password = bcrypt($request->password);
            $pengguna->role = $request->role;
            $pengguna->status = $request->status;
            $pengguna->save();

            // Buat profil baru
            $profil = new Profil;
            $profil->id_pengguna = $pengguna->id;
            $profil->nama_lengkap = $request->nama_lengkap;
            $profil->no_telepon = $request->no_telepon;
            $profil->save();

            // Redirect ke halaman yang sesuai setelah create sukses
            return back()->with('success', 'Pengguna dan profil berhasil ditambahkan');
        }
    

    public function update(Request $request)
    {
        try {

            // Validasi input jika diperlukan
            $request->validate([
                'email' => 'required|email|unique:pengguna,email,' . $request->id,
                'role' => 'required|in:Super Admin,Admin,Teknisi',
                'status' => 'required|in:Aktif,Tidak Aktif',
            ], [
                'email.required' => 'Email wajib diisi.',
                'email.email' => 'Format email tidak valid.',
                'email.unique' => 'Email sudah terdaftar.',
                'role.required' => 'Role wajib diisi.',
                'role.in' => 'Role tidak valid. Pilihan yang valid adalah: Super Admin, Admin, Teknisi.',
                'status.required' => 'Status wajib diisi.',
                'status.in' => 'Status tidak valid. Pilihan yang valid adalah: Aktif, Tidak Aktif.',
            ]);

            // Cari pengguna berdasarkan ID
            $pengguna = Pengguna::findOrFail($request->id);

            // Update data pengguna berdasarkan data yang dikirim dari form
            $pengguna->email = $request->email;
            if($request->password != null){
                $pengguna->password = bcrypt($request->password);
            }
            $pengguna->role = $request->role;
            $pengguna->status = $request->status;
            $pengguna->save();

            // Redirect ke halaman yang sesuai setelah update sukses

            return response()->json(['success' => 'Pengguna berhasil diupdate']);

            // return redirect()->route('sprpengguna')->with('success', 'Pengguna berhasil diupdate');
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage() ], 500);
        }
    }

    

    function destroy(Request $request)
    {
        try {
            Profil::destroy($request->dataHapus);
            Pengguna::destroy($request->dataHapus);
            // Hapus pengguna

            // Redirect ke halaman yang sesuai setelah delete sukses
            return response()->json(['success' => 'Pengguna berhasil dihapus']);
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage() ], 500);
        }
    }
}
