<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tiket;
use App\Models\Profil;
use App\Models\RiwayatTiket;
use Yajra\DataTables\Facades\DataTables;

class TiketKaryawanController extends Controller
{
    public function lihatTiketKaryawan(Request $request)
    {
        if ($request->ajax()) {
            $tiketKaryawan = Tiket::whereNotNull('nik_karyawan')->with('pengguna.profil', 'lampiran')->get()->map(function ($tiket) {
                foreach ($tiket->getAttributes() as $key => $value) {
                    if ($value === null) {
                        $tiket->{$key} = '---';
                    }
                }

                if ($tiket->id_pengguna == '---') {
                    $tiket->aksi = 'Button';
                } elseif ($tiket->id_pengguna != '---') {
                    $tiket->aksi = '---';
                }

                $tiket->waktu_tiket = date_format(date_create($tiket->waktu_tiket), 'Y/m/d');

                $tiket->nama_lengkap = $tiket->pengguna->profil->nama_lengkap ?? '---';

                $tiket->file_tiket = $tiket->lampiran->first()->file_tiket ?? '---';

                $tiket->unit_kerja_id = $tiket->unit_kerja->unit_kerja_karyawan;

                return $tiket;
            });
            return Datatables::of($tiketKaryawan)->make(true);
        }

        $namaPekerjaTeknisi = Profil::join('pengguna', 'profil.id_pengguna', '=', 'pengguna.id')
            ->where('pengguna.role', 'Teknisi')
            ->get(['profil.*']);

        return view('admin/tiket_karyawan', [
            'namaPekerjaTeknisi' => $namaPekerjaTeknisi,
        ]);
    }

    public function kirimTiketKaryawan(Request $request)
    {
        $idPengguna = $request->input('idPengguna');
        $tiketId = $request->input('tiketId');
        $tiketTeknisi = Tiket::findOrFail($tiketId);
        $namaTeknisi = Profil::findOrFail($idPengguna)->nama_lengkap; // (Mengambil Nama)
        $tiket = $tiketTeknisi->update([
            'id_pengguna' => $idPengguna,
            'tanggal_masuk' => now(),
        ]);

        $riwayatTiket = RiwayatTiket::create([
            'id_pengguna' => $idPengguna,
            'id_tiket' => $tiketTeknisi->id,
            'waktu_riwayat' => now(),
            'deskripsi_riwayat' => 'Laporan dikirim ke teknisi bernama ' . $namaTeknisi . '', // (Menunjukkan Nama)
        ]);

        if ($tiket && $riwayatTiket) {
            return response()->json([
                'success' => true,
                'message' => 'Tiket Terkirim Ke ' . $namaTeknisi . '!',
            ]);
        }
    }
}
